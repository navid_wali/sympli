﻿using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;

namespace SeoReport.Proxies
{
    public class MockGoogleProxy : IGoogleProxy
    {
        private IHostingEnvironment _env;

        public MockGoogleProxy(IHostingEnvironment env)
        {
            _env = env;
        }

        public Task<string> GetSearchResults(string query)
        {
            return System.IO.File.ReadAllTextAsync(System.IO.Path.Combine(_env.ContentRootPath, @"AppData\googleResponse.txt"));
        }
    }
}
