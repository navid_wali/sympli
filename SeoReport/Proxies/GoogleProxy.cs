﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace SeoReport.Proxies
{
    public interface IGoogleProxy
    {
        Task<string> GetSearchResults(string query);
    }

    public class GoogleProxy : IGoogleProxy
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;
        private const string otherParams = "cr=countryAU&lr=lang_en&client=google-csbe";

        public GoogleProxy(HttpClient client, ILogger<GoogleProxy> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task<string> GetSearchResults(string query)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"?{query}&{otherParams}");
            var response = await _client.SendAsync(request);
            // ok to let the exception bubble up and ultimately return a 500
            // response.EnsureSuccessStatusCode();
            if(response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                _logger.LogError($"Error getting data from google, status code: {response.StatusCode}");
                throw new HttpRequestException("Error getting data from google");
            }
        }
    }
}
