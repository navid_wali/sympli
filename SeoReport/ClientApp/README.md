## Development

Run `npm start` to start client app on http://localhost:3000
This will route all API requests to http://localhost:5000 - this value is set in .env.development
Note that this means the net core roject needs to be running on that port

## Production

Run `npm run build` to generate production build. Make sure to set the correct .env.production value 
for the API URL. 

## Tests

Run `npm run test` to run tests