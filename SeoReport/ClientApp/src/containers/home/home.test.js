import React from 'react';
import { shallow } from 'enzyme';
import { Home } from './home';
import * as helper from '../../utils/helpers';

describe("tests for the Home component", () => {
    
    beforeEach(() => {
        jest.spyOn(helper, 'getAppearancesInSearch')
            .mockReturnValue(Promise.resolve([1,2,3]));
    });

    afterEach(() => {
        helper.getAppearancesInSearch.mockClear();
    })

    it("renders Home component", async () => {
        const wrapper = await shallow(<Home />);
        expect(helper.getAppearancesInSearch).toHaveBeenCalledTimes(1);
        expect(wrapper.find(".result").html().indexOf("1, 2, 3")).toBeGreaterThanOrEqual(0);
    });

    it("calls getAppearancesInSearch after refresh", async () => {
        const wrapper = await shallow(<Home />);
        wrapper.instance().refreshResults();
        expect(helper.getAppearancesInSearch).toHaveBeenCalledTimes(2);
    });

    it("cached set to true when refreshResults is clicked", async () => {
        const wrapper = await shallow(<Home />);
        wrapper.instance().refreshResults();
        // get eh args for the second call
        const { cached } = helper.getAppearancesInSearch.mock.calls[1][0];
        expect(cached).toBeTruthy();
    });

    it("calls getAppearancesInSearch after refreshCacheResults", async () => {
        const wrapper = await shallow(<Home />);        
        wrapper.instance().refreshCacheResults();
        expect(helper.getAppearancesInSearch).toHaveBeenCalledTimes(2);
    });

    it("cached set to false when refreshCacheResults is clicked", async () => {
        const wrapper = await shallow(<Home />);
        wrapper.instance().refreshCacheResults();
        // get eh args for the second call
        const { cached } = helper.getAppearancesInSearch.mock.calls[1][0];
        expect(cached).toBeFalsy();
    });

    it("shows 0 when appearesIn does not return valid result", async () => {
        jest.spyOn(helper, 'getAppearancesInSearch')
            .mockReturnValue(Promise.resolve(null));
        const wrapper = await shallow(<Home />);
        expect(wrapper.find(".result").html().indexOf("0")).toBeGreaterThanOrEqual(0);
    });
})