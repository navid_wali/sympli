import React, { Component } from 'react';
import { getAppearancesInSearch, formatArray } from '../../utils/helpers';
import constants from '../../utils/constants';
import { Search } from '../../components/search/search';
import './home.scss';
const { providers, search } = constants;

export class Home extends Component {
  state = {
    appearsIn: [],
    provider: providers.default,
    searchUrl: search.defaultUrl,
    searchTerm: search.defaultTerm,
    results: search.defaultResults
  };

  componentDidMount = async () => await this.getSearchReport();
  
  refreshResults = async () => await this.getSearchReport();

  refreshCacheResults = async () => await this.getSearchReport(false);

  onChange = prop => ({target: { value }}) => this.setState({ [prop] : value });

  getSearchReport = async (cached=true) => {
    const { provider, searchUrl, searchTerm, results } = this.state;
    try
    {
      this.setState({      
        appearsIn: await getAppearancesInSearch({ 
          provider, 
          searchUrl, 
          searchTerm, 
          results, 
          cached 
        })
      });
    } catch {
      this.setState({ appearsIn: [] }); // todo: can do error handling here
    }
  }
  
  render() {
    const { appearsIn, provider, searchUrl, searchTerm } = this.state;
    return (
      <>
        <Search 
          provider={provider} 
          onProviderChange={this.onChange('provider')}
          searchUrl={searchUrl} 
          onSearchUrlChange={this.onChange('searchUrl')}
          searchTerm={searchTerm} 
          onSearchTermChange={this.onChange('searchTerm')}
          onRefreshClicked={this.refreshResults}
          onRefreshCacheClicked={this.refreshCacheResults}
        />
        <div>
        Result positions of <strong>{searchUrl}</strong> in <strong>{provider}</strong> when searching <strong>{searchTerm}</strong>:
         <br />
         <span className="result">{formatArray(appearsIn)}</span>
         </div>
      </>
    );
  }
}