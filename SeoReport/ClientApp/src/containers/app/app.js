import React, { Component } from 'react';
import { Layout } from '../../components/Layout';
import { Home } from '../home/home';

export default class App extends Component {
  render() {
    return (
      <Layout>
        <Home />
      </Layout>
    );
  }
}
