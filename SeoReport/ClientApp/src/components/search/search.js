import React from 'react';
import constants from '../../utils/constants';
const { providers } = constants;

export const Search = (
    {
        provider,
        onProviderChange,
        searchUrl,
        onSearchUrlChange,
        searchTerm,
        onSearchTermChange,
        onRefreshClicked,
        onRefreshCacheClicked
    }
) => {
    return (
        <ul>
            <li>
                Provider: 
                <select value={provider} onChange={onProviderChange}>
                    {providers.available
                        .map((opt, index) => <option key={index}>{opt}</option>)}
                </select>
            </li>
            <li>
                Search for term: 
                <input type="text" value={searchTerm} onChange={onSearchTermChange} />
            </li>
            <li>
                Search for match: 
                <input type="text" value={searchUrl} onChange={onSearchUrlChange} />
            </li>         
            <li>
                <button onClick={onRefreshClicked}>Refresh results</button>
            </li>
            <li>
                <button onClick={onRefreshCacheClicked}>Refresh cache</button>
            </li>            
        </ul>
    )
}