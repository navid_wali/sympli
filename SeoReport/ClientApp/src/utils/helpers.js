import { getParserForProvider } from './searchParsers';
import constants from '../utils/constants';
const { providers, search } = constants;

export const getAppearancesInSearch = async ({
        provider = providers.default,
        searchUrl = search.defaultUrl,
        searchTerm = search.defaultTerm,
        results = search.defaultResults,
        cached = true
    } = {}) => {

    const text = await getFromServer({ provider, searchTerm, results, cached });
    return getParserForProvider(provider)(text, searchUrl);
}

export const formatArray = arr => {
    if(!!arr && Array.isArray(arr) && arr.length > 0) return arr.join(", ");
    return "0";
};

const getFromServer = async ({path="api/search/", provider, searchTerm, results, cached}) => {
    const fullUrl = `${process.env.REACT_APP_API_URL}/${path}?provider=${provider}&query=${searchTerm}&results=${results}&cached=${cached}`;
    const response = await fetch(fullUrl);
    const { ok } = response;
    if(ok) {
        return await response.text();
    } 
    throw new Error("Could not get response from server");
};