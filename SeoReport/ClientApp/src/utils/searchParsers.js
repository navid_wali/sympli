import constants from './constants';
const { providers } = constants;

export const getParserForProvider = provider => {
    switch(provider) {
        case providers.google:
            return googleResultsParser;
        default: return googleResultsParser;
    }
}

const googleResultsParser = (text, search) => {
    if(!search || !text) return [];
    const dom = new DOMParser().parseFromString(text, 'text/html');
    const els = dom.getElementsByClassName('g');
    let appearsIn = [];
    let position = 0;
    for(const index in els) {
        const el = els[index];
        if(!!el && el.nodeName === 'DIV') {
            const a = el.getElementsByTagName('a');            
            if(!!a && a.length > 0) {
                if(a[0].href.indexOf(search) >= 0) {
                    appearsIn.push(position+1);
                }
            }
        }
        position = position + 1;
    }
    return appearsIn;    
}

// add more parsers here as required...