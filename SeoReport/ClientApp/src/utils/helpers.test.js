import { getAppearancesInSearch, formatArray } from './helpers';

describe("formatArray tests", () => {
    const testCases = [
        {testCase: [1, 2, 3], result: "1, 2, 3"},
        {testCase: null, result: "0"},
        {testCase: undefined, result: "0"},
        {testCase: [], result: "0"},
        {testCase: "[1, 2, 3]", result: "0"}
    ];
    it("returns formatted array", () => {
        testCases.forEach( ({testCase, result}) => expect(formatArray(testCase)).toEqual(result));
    })
});

describe("get appearances in search tests", () => {
    // todo: move this to shared variable
    const mockData = `
    <!doctype html>
    <html>
        <body>
            <p>Other content>
            <div class="g">
                <a href="sympli.com.au">link</a>
            </div>
            <div class="g">
                <a href="somethingelse.com.au">link</a>
            </div>
            <div class="nested">
                <div class="g">
                    <a href="http://redirector/?=sympli.com.au">link</a>
                </div>            
            </div>
        </body>
    </html>`;
    const mockTextPromise = Promise.resolve(mockData);
    const mockFetchPromise = Promise.resolve({
      text: () => mockTextPromise,
      ok:  true
    });
    beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
    });

    it("should call fetch once to get page from server", async done => {
        await getAppearancesInSearch()
        expect(global.fetch).toHaveBeenCalledTimes(1);
        process.nextTick(() => {
            global.fetch.mockClear();
            done();
        });
    });

    it("fetch url path should be correct", async done => {
        const testPath = "/api/search/?provider=google&query=e-settlements&results=100&cached=true";
        await getAppearancesInSearch();
        // check argument passed in for fetch
        const urlArg = global.fetch.mock.calls[0][0];
        expect(urlArg.indexOf(testPath)).toBeGreaterThanOrEqual(0);
        process.nextTick(() => {
            global.fetch.mockClear();
            done();
        });
    });

    it("if server responds with not ok then throws error", async done => {
        // override 
        const mockFetchPromise = Promise.resolve({            
            ok:  false
        });        
        jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

        await expect(getAppearancesInSearch()).rejects.toThrow();

        process.nextTick(() => {
            global.fetch.mockClear();
            done();
        });
    })

});