export default {
    providers: {
        default: 'google',
        available: ['google'],
        google: 'google'
    },
    search: {
        defaultTerm: 'e-settlements',
        defaultUrl: 'sympli.com.au',
        defaultResults: 100
    }
}