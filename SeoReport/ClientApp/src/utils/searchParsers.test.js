import { getParserForProvider } from './searchParsers';

describe("searchParser factory tests", () => {
    it("should return the right parser", () => {
        const parser = getParserForProvider("google");
        expect(parser).not.toBeNull();
        expect(parser).toBeInstanceOf(Function);
        expect(parser.name).toBe("googleResultsParser");
    });

    it("should return the google parser by default", () => {
        const parser = getParserForProvider();
        expect(parser).not.toBeNull();
        expect(parser).toBeInstanceOf(Function);
        expect(parser.name).toBe("googleResultsParser");        
    })
});

const mockData = `
    <!doctype html>
    <html>
        <body>
            <p>Other content>
            <div class="g">
                <a href="sympli.com.au">link</a>
            </div>
            <div class="g">
                <a href="somethingelse.com.au">link</a>
            </div>
            <div class="nested">
                <div class="g">
                    <a href="http://redirector/?=sympli.com.au">link</a>
                </div>            
            </div>
        </body>
    </html>`;

describe("google result parser", () => {
    it("should return the right result based on mock data", () => {
        const result = getParserForProvider("google")(mockData, "sympli.com.au");
        expect(result).toHaveLength(2);
        expect(result).toEqual([1, 3]);
    });

    it("should return empty when input text empty", () => {
        const result = getParserForProvider("google")("", "sympli.com.au");
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);                
    });

    it("should return empty when input text null", () => {
        const result = getParserForProvider("google")(null, "sympli.com.au");
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);
    });

    it("should return empty when input text undefined", () => {
        const result = getParserForProvider("google")(undefined, "sympli.com.au");
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);                
    });

    it("should return empty when input searchUrl empty or null", () => {
        const result = getParserForProvider("google")(mockData, "");
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);
    });

    it("should return empty when input searchUrl null", () => {
        const result = getParserForProvider("google")(mockData, null);
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);        
    });

    it("should return empty when input searchUrl undefined", () => {
        const result = getParserForProvider("google")(mockData, undefined);
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);        
    });

    it("should return empty when input text is wrong type", () => {
        const result = getParserForProvider("google")({mockData}, "sympli.com.au");
        expect(result).toHaveLength(0);
        expect(result).toEqual([]);
    });    
})
