using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SeoReport.Proxies;
using SeoReport.Services;
using System;

namespace SeoReport
{
    /// <summary>
    /// Extension class to add various search result providers
    /// </summary>
    public static class SeoReportIOCConfiguration
    {
        public static void AddSearchProviders(this IServiceCollection services)
        {
            services.AddTransient<GoogleSearchProvider>();

            services.AddTransient<Func<string, ISearchProvider>>(searchProvider => key =>
            {
                switch (key)
                {
                    case "google":
                        return searchProvider.GetService<GoogleSearchProvider>();
                    default:
                        return searchProvider.GetService<GoogleSearchProvider>();
                }
            });
        }

        public static void AddHttpClients(this IServiceCollection services, IConfiguration config)
        {
            services
                .AddHttpClient<IGoogleProxy, GoogleProxy>(c =>
                {
                    c.BaseAddress = new Uri(config.GetValue<string>("providers:google:baseUrl"));
                    c.Timeout = TimeSpan.FromSeconds(config.GetValue<double>("providers:google:timeout"));
                });
        }

    }        

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddMemoryCache();

            // Services
            services.AddSearchProviders();

            // Add proxies
            services.AddHttpClients(Configuration);

            services
                // .AddSingleton<IGoogleProxy, MockGoogleProxy>()
                .AddSingleton<ICacheService, CacheService>();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddCors(o => o.AddPolicy("DevPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors("DevPolicy");
            }
            else
            {
                app.UseHttpsRedirection();
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
            });
        }
    }
}
