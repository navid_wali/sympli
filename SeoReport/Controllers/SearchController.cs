﻿using Microsoft.AspNetCore.Mvc;
using SeoReport.Services;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SeoReport.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        // use service accessor to locate provider
        private readonly Func<string, ISearchProvider> _serviceAccessor;

        public SearchController(Func<string, ISearchProvider> serviceAccessor)
        {
            _serviceAccessor = serviceAccessor;
        }

        /// <summary>
        /// Return response from search providers
        /// </summary>
        /// <param name="provider">provider name. defaults to google - can use this to extend to other search providers</param>
        /// <param name="query">defaults to e-settlements, but other terms can be queries</param>
        /// <param name="results">number of results to return. defaults to 100</param>
        /// <param name="cached">return result from cache. if false then get a fresh copy of data from provider</param>
        /// <returns></returns>
        public Task<string> Get(string provider="google", string query="e-settlements", int results=100, bool cached=true)
        {
            return _serviceAccessor(provider).Response(WebUtility.UrlEncode(query), results, cached);
        }
    }
}