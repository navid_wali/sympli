﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace SeoReport.Services
{
    public interface ICacheService
    {
        Task<string> GetAsync(string key, Func<Task<string>> getFreshData);
        void Remove(string key);
    }

    public class CacheService : ICacheService
    {
        private readonly IMemoryCache _cache;

        public CacheService(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task<string> GetAsync(string key, Func<Task<string>> getFreshData)
        {
            string result = string.Empty;
            if (!_cache.TryGetValue(key, out result))
            {
                result = await getFreshData();
                var options = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(1)); // TODO: load this from config
                _cache.Set(key, result, options);
            }
            return result;
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }
    }
}
