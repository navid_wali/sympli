﻿using System.Threading.Tasks;

namespace SeoReport.Services
{
    public interface ISearchProvider
    {
        Task<string> Response(string query, int results, bool fromCache);
    }
}
