﻿using SeoReport.Proxies;
using System.Threading.Tasks;

namespace SeoReport.Services
{
    public class GoogleSearchProvider : ISearchProvider
    {
        private readonly IGoogleProxy _proxy;
        private readonly ICacheService _cacheService;
        const string CacheKey = "google";

        public GoogleSearchProvider(IGoogleProxy proxy, ICacheService cacheService)
        {
            _proxy = proxy;
            _cacheService = cacheService;
        }

        public Task<string> Response(string query, int results, bool fromCache=true)
        {
            if (!fromCache) _cacheService.Remove(CacheKey);
            return _cacheService.GetAsync(CacheKey, async () =>
            {
                return await GetResponseFromProxy(query, results);
            });
        }

        private Task<string> GetResponseFromProxy(string query, int results)
        {
            return _proxy.GetSearchResults($"q={query}&start=0&num={results}");
        }
    }
}
