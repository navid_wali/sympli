﻿using FluentAssertions;
using Moq;
using SeoReport.Proxies;
using SeoReport.Services;
using System;
using System.Threading.Tasks;
using Xunit;

namespace SeoReport.UnitTests.Services
{
    public class GoogleSearchProviderTests
    {
        private readonly Mock<IGoogleProxy> _mockProxy;
        private readonly Mock<ICacheService> _cacheService;
        private readonly GoogleSearchProvider _service;

        const string searchResponse = "search response";

        public GoogleSearchProviderTests()
        {
            _mockProxy = new Mock<IGoogleProxy>();
            _cacheService = new Mock<ICacheService>();

            _mockProxy.Setup(p => p.GetSearchResults(It.IsAny<string>()))
                .Returns(Task.FromResult(searchResponse));
            _cacheService.Setup(c => c.GetAsync(It.IsAny<string>(), It.IsAny<Func<Task<string>>>()))
                .Returns(Task.FromResult(searchResponse));

            _service = new GoogleSearchProvider(_mockProxy.Object, _cacheService.Object);
        }

        [Fact]
        public async Task Response_ReturnsResponse()
        {
            var result = await _service.Response("", 0);
            result.Should().NotBeNullOrEmpty();
            result.Should().Match(searchResponse);
        }

        [Fact]
        public async Task Response_IfInCacheReturnsFromCache()
        {
            // arrange
            // make proxy return a different value to the cache
            _mockProxy.Setup(p => p.GetSearchResults(It.IsAny<string>()))
                .Returns(Task.FromResult("something else"));

            var result = await _service.Response("", 0);
            result.Should().NotBeNullOrEmpty();
            result.Should().Match(searchResponse);
        }
    }
}
