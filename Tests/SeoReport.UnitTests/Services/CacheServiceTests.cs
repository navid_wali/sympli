﻿using FluentAssertions;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using SeoReport.Services;
using System.Threading.Tasks;
using Xunit;

namespace SeoReport.UnitTests.Services
{    
    public class CacheServiceTests
    {
        private readonly Mock<IMemoryCache> _mockCache;
        private readonly CacheService _service;
        private object cachedResult = "cacheResult";
        private readonly string notCacheResult = "cacheResult";

        public CacheServiceTests()
        {
            _mockCache = new Mock<IMemoryCache>();
            var cachEntry = Mock.Of<ICacheEntry>();
            _mockCache.Setup(c => c.TryGetValue(It.IsAny<string>(), out It.Ref<object>.IsAny))
                .Returns(false); // not in cache
            _mockCache.Setup(m => m.CreateEntry(It.IsAny<object>()))
                .Returns(cachEntry);
            _service = new CacheService(_mockCache.Object);
        }

        [Fact]
        public async Task GetAsync_ReturnsResultFromCache()
        {            
            _mockCache.Setup(c => c.TryGetValue(It.IsAny<string>(), out cachedResult))
                .Returns(true); // in cache
            var result = await _service.GetAsync("test", () => Task.FromResult(notCacheResult));

            result.Should().NotBeNullOrEmpty();
            result.Should().Match((string)cachedResult);
        }

        [Fact]
        public async Task GetAsync_ReturnsResultFromLambdaFunction()
        {
            var result = await _service.GetAsync("test", () => Task.FromResult(notCacheResult));

            result.Should().NotBeNullOrEmpty();
            result.Should().Match(notCacheResult);
        }
    }
}
