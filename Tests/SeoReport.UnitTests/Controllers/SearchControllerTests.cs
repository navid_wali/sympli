﻿using Moq;
using SeoReport.Controllers;
using SeoReport.Services;
using System;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace SeoReport.UnitTests.Controllers
{
    public class SearchControllerTests
    {
        private readonly Mock<Func<string, ISearchProvider>> _serviceAccessor;
        private readonly Mock<ISearchProvider> _mockSearchProvider;
        private readonly SearchController _controller;
        const string mockSearchResponse = "mock response";

        public SearchControllerTests()
        {
            _serviceAccessor = new Mock<Func<string, ISearchProvider>>();            
            _mockSearchProvider = new Mock<ISearchProvider>();
            _mockSearchProvider.Setup(s => s.Response(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(mockSearchResponse));
            _serviceAccessor.Setup(s => s(It.IsAny<string>()))
                .Returns(_mockSearchProvider.Object);
            _controller = new SearchController(_serviceAccessor.Object);
        }

        [Fact]
        public async Task Search_ReturnsSearchResults()
        {
            // Act
            var result = await _controller.Get();
            result.Should().NotBeNullOrEmpty();
            result.Should().Match(mockSearchResponse);
        }
    }
}
