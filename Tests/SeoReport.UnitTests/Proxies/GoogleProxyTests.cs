﻿using Moq;
using SeoReport.Proxies;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using System.Threading;
using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace SeoReport.UnitTests.Proxies
{
    public class GoogleProxyTests
    {
        private readonly HttpClient _httpClient;
        private GoogleProxy _proxy;
        private Mock<ILogger<GoogleProxy>> _logger;
        const string responseText = "response";

        class MockHttpHandler : HttpMessageHandler
        {
            private readonly HttpStatusCode _returnStatusCode;
            private readonly string _returnResponse;
            public MockHttpHandler(HttpStatusCode returnStatusCode, string returnResponse)
            {
                _returnStatusCode = returnStatusCode;
                _returnResponse = returnResponse;
            }

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                return Task.FromResult(new HttpResponseMessage(_returnStatusCode)
                {
                    Content = new StringContent(_returnResponse)
                });
            }
        }

        public GoogleProxyTests()
        {
            _httpClient = new HttpClient(new MockHttpHandler(HttpStatusCode.Accepted, responseText))
            {
                BaseAddress = new Uri("http://mock/")
            };
            _logger = new Mock<ILogger<GoogleProxy>>();
            _proxy = new GoogleProxy(_httpClient, _logger.Object);
        }        

        [Fact]
        public async Task GetSearchResults_ReturnsResultWhenSuccessful()
        {
            var result = await _proxy.GetSearchResults(string.Empty);
            result.Should().NotBeNullOrEmpty();
            result.Should().Match(responseText);
        }

        [Fact]
        public async Task GetSearchResults_ThrowsExceptionIfNotSuccesful()
        {
            // Arrange
            var httpClient = new HttpClient(new MockHttpHandler(HttpStatusCode.BadRequest, responseText))
            {
                BaseAddress = new Uri("http://mock/")
            };
            var proxy = new GoogleProxy(httpClient, _logger.Object);

            // Act & Assert
            await Assert.ThrowsAsync<HttpRequestException>(() => proxy.GetSearchResults(string.Empty));
        }

    }
}
