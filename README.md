## Development

From command line: Run `dotnet run` to start server app on http://localhost:5000 and https://localhost:5001 and check the clientapp readme for instructions on how to run client

### To run from Visual Studio

Build the client side app in production mode first (`clientapp\npm run build`) and then debug 
from Visual Studio. The .env.production file in clientapp has the URL for API and ensure its set
to the same URL as .env.production (currently `https://localhost:44327/`)

## Production

Run `dotnet publish` to generate production build. Make sure to read the readme for the clientapp
for instructions on how to set the clientapp up for Production

## Tests

Test projects (Unit and Integration) are in `Tests` folder

## Making the app scalable

### Performance

The application can be split into a front end component and back end component, meaning each individual component can be deployed separately if required. This would allow the application to be scaled out and wide if required.
The application can also be containerised for more distributed deployment.

In terms of code improvements to help with performance, a faster cache can potentially be used (e.g. Redis), along with logner cache times. Queries can also be made against search services to inspect the first N number of results 
(in a background job) to see if there are changes in results, and only update the cache if there is a change.

### Availability

Generally splitting the front end and back end, and deploying it across muktiple nodes should address availability concerns. However it can also be deployed across multiple cloud providers and geographical centres to ensure continuity
of services. The infrastructure would need to have the ability to recover based on agreed upon RTO and RPO - although with this app the RPO might not be as stringest a requirement as it is only getting data from third party sources.

### Reliability

The reliability of the app can be improved by ensuring caches can be extended in case search providers are temporarily unavailable. In addition, to be able to let the user know during run time which services are and are not available 
would ensure the correct expectations are being set for the user. The data in this instance can't really be archived and ready for restore as it will be too old and lose its relevance. Applying the principles above re: availability and 
performance would also lead to a more reliable app.